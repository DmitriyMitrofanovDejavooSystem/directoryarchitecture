package com.example.architecture.mvp_user_login.ui;

/**
 * Created by Dmitriy Mitrofanov on 16.01.2018.
 *
 * User login router contract.
 */

public interface UserLoginRouterContract {
    //route to next screen
    void onSuccessLogin(String userId);

    //route to lost password
    void onLostPassword(String userId);
}
