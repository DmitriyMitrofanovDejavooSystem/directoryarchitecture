package com.example.architecture.mvp_settings_list.ui;

/**
 * Created by Dmitriy Mitrofanov on 16.01.2018.
 */

public interface SettingsListRouterContract {
    void routeToPrinterList();
}
