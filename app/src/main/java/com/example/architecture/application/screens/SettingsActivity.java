package com.example.architecture.application.screens;

import android.support.v7.app.AppCompatActivity;

import com.example.architecture.mvp_printer_list.ui.PrinterListRouterContract;
import com.example.architecture.mvp_settings_list.ui.SettingsListRouterContract;

/**
 * Created by Dmitriy Mitrofanov on 16.01.2018.
 */

public class SettingsActivity extends AppCompatActivity implements SettingsListRouterContract, PrinterListRouterContract {

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        //attach settings fragment
    }

    @Override
    public void routeToEditPrinter(String printerId) {
        //show fragment or dialog fragment with printer edit
        //or start new activity with edit printer
    }

    @Override
    public void routeToPrinterList() {
        //show fragment or dialog fragment with printer list
        //or start new activity with printer list
    }
}
