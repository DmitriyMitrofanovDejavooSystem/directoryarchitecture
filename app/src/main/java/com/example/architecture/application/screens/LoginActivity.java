package com.example.architecture.application.screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.example.architecture.mvp_user_login.ui.UserLoginRouterContract;

/**
 * Created by Dmitriy Mitrofanov on 16.01.2018.
 *
 * Login activity
 */

public class LoginActivity extends AppCompatActivity implements UserLoginRouterContract {
    @Override
    public void onSuccessLogin(String userId) {
        Intent intent = new Intent(this,SettingsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onLostPassword(String userId) {

    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        //Attach login fragment
    }
}
